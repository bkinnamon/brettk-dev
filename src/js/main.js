const mediaQuery = 'screen and (max-width: 699px)';
const defaultMargin = '4rem';
const smallMargin = '1rem'

const projectButtons = document.querySelectorAll(".project");
const backdrop = document.querySelector(".backdrop");
const closeBtn = document.querySelector(".btn-close");

const placeholder = document.createElement("div");
placeholder.classList.add("project");

const openStyles = {
  position: "fixed",
  zIndex: 100,
};

const setMargin = (margin) => {
    openStyles.left = margin;
    openStyles.top = margin;
    openStyles.width = `calc(100vw - ${margin} - ${margin})`
    openStyles.height = `calc(100vh - ${margin} - ${margin})`
}

const mql = window.matchMedia(mediaQuery);
mql.addListener((event) => {
  if (event.matches) {
    setMargin(smallMargin)
  } else {
    setMargin(defaultMargin)
  }
});
if (mql.matches) {
  setMargin(smallMargin)
} else {
  setMargin(defaultMargin)
}

let resetStyles = {};

let openProject = null;

const close = () => {
  backdrop.classList.remove("open");
  openProject.classList.remove("open")

  if (!openProject) return;

  openProject.addEventListener(
    "transitionend",
    () => {
      openProject.attributes.removeNamedItem("style");
      placeholder.replaceWith(openProject);
    },
    { once: true }
  );
  Object.assign(openProject.style, resetStyles);
}

projectButtons.forEach((button) => {
  button.addEventListener("click", () => {
    if (backdrop.classList.contains("open")) {
      close();
    } else {
      const rect = button.getBoundingClientRect();
      resetStyles = {
        left: `${rect.left}px`,
        top: `${rect.top}px`,
        width: `${rect.width}px`,
        height: `${rect.height}px`,
        zIndex: 0,
      };
      Object.assign(button.style, resetStyles);
      
      button.replaceWith(placeholder);
      document.body.append(button);
      openProject = button;

      window.requestAnimationFrame(() => {
        backdrop.classList.add("open");
        openProject.classList.add("open")
        Object.assign(button.style, openStyles);
      });
    }
  });
});

closeBtn.addEventListener("click", close);
