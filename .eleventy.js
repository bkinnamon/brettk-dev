const CleanCSS = require("clean-css");

module.exports = (config) => {
  config.addFilter("cssmin", (code) => {
    return new CleanCSS({}).minify(code).styles;
  });

  config.addPassthroughCopy("src/images");
  config.addPassthroughCopy("src/js");
};
